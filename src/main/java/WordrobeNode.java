import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.io.IOException;

public class WordrobeNode {
    public static void main(String[] args) throws IOException {
        Config cfg = new Config();
        cfg.getGroupConfig().setName("Wordrobe");
        cfg.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        cfg.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(true).addMember("127.0.0.1");;
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
    }
}