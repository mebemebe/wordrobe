import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICompletableFuture;
import com.hazelcast.core.IMap;
import com.hazelcast.map.AbstractEntryProcessor;
import com.hazelcast.mapreduce.Collator;
import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;
import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import com.hazelcast.mapreduce.Mapper;
import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

public class WordrobeClient {
    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        ClientConfig cfg = new ClientConfig();
        cfg.getGroupConfig().setName("Wordrobe");
        HazelcastInstance client = HazelcastClient.newHazelcastClient(cfg);

        IMap<String, String> corpora = client.getMap("corpora");

        for (final File corpus : (new File("./corpora")).listFiles()) {
            corpora.put(corpus.getName().split("\\.")[0], new String(Files.readAllBytes(corpus.toPath())));
        }

        Map<String, Object> wordCounts = corpora.executeOnEntries(new WordCounterEntryProcessor());

        long total = 0;
        for (Object count : wordCounts.values()) {
            total += (Long)count;
        }

        System.out.println("Total words " + total);

        JobTracker jobTracker = client.getJobTracker("distinctor");

        KeyValueSource<String, String> source = KeyValueSource.fromMap(corpora);
        Job<String, String> job = jobTracker.newJob(source);

        ICompletableFuture<Long> future = job
                .mapper(new TokenizerMapper())
                .combiner(new WordCountCombinerFactory())
                .reducer(new WordCountReducerFactory())
                .submit(new WordCountCollator());

        System.out.println("Total words " + future.get());
    }

    public static class WordCounterEntryProcessor extends AbstractEntryProcessor<String, String> {
        public WordCounterEntryProcessor() {
            super(false);
        }

        @Override
        public Long process(Map.Entry<String, String> entry) {
            System.out.println("Counting " + entry.getKey());

            long count = 0;
            final StringTokenizer tokenizer = new StringTokenizer(entry.getValue());
            while (tokenizer.hasMoreTokens()) {
                count++;
                tokenizer.nextToken();
            }

            return count;
        }
    }

    public static class TokenizerMapper implements Mapper<String, String, String, Long> {
        @Override
        public void map(String key, String document, Context<String, Long> context) {
            System.out.println("Tokenizing " + key);
            StringTokenizer tokenizer = new StringTokenizer(document.toLowerCase());
            while (tokenizer.hasMoreTokens()) {
                context.emit(tokenizer.nextToken(), 1L);
            }
        }
    }

    public static class WordCountCombinerFactory implements CombinerFactory<String, Long, Long> {

        @Override
        public Combiner<String, Long, Long> newCombiner(String key) {
            return new WordCountCombiner();
        }

        private class WordCountCombiner extends Combiner<String, Long, Long> {
            private long sum = 0;

            @Override
            public void combine(String key, Long value) {
                sum++;
            }

            @Override
            public Long finalizeChunk() {
                long chunk = sum;
                sum = 0;
                return chunk;
            }
        }
    }

    public static class WordCountReducerFactory implements ReducerFactory<String, Long, Long> {

        @Override
        public Reducer<String, Long, Long> newReducer(String key) {
            return new WordCountReducer();
        }

        private class WordCountReducer extends Reducer<String, Long, Long> {

            private volatile long sum = 0;

            @Override
            public void reduce(Long value) {
                sum += value.longValue();
            }

            @Override
            public Long finalizeReduce() {
                return sum;
            }
        }
    }

    public static class WordCountCollator implements Collator<Map.Entry<String, Long>, Long> {

        @Override
        public Long collate(Iterable<Map.Entry<String, Long>> values) {
            long sum = 0;

            for (Map.Entry<String, Long> entry : values) {
                sum += entry.getValue().longValue();
            }
            return sum;
        }
    }


}
